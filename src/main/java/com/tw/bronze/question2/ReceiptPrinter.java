package com.tw.bronze.question2;

import java.util.HashMap;
import java.util.List;

public class ReceiptPrinter {
    private final HashMap<String, Product> products =
        new HashMap<>();

    public ReceiptPrinter(List<Product> products) {
        // TODO:
        //   Please implement the constructor
        // <-start-
        for (Product product : products) {
            this.products.put(product.getId(), product);
        }
    }
        // --end->

    public int getTotalPrice(List<String> selectedIds) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (selectedIds == null){
            throw new IllegalArgumentException("if_selected_product_is_null");
        } else if (selectedIds.isEmpty()){
            return 0;
        } else {
            int totalPrice = 0;
            for (String selectedId : selectedIds){
                totalPrice += this.products.get(selectedId).getPrice();
            }
            return totalPrice;
        }
        // --end->
    }
}

