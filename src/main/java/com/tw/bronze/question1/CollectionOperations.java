package com.tw.bronze.question1;

import java.util.*;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null){
            throw new IllegalArgumentException("array_is_null");
        }
        HashSet<String> removeDuplicates = new HashSet<>();
        for (String content: values){
            removeDuplicates.add(content);
        }
        ArrayList<String> sortedString = new ArrayList<>(removeDuplicates);
        Collections.sort(sortedString);
        return sortedString.toArray(new String[0]);

        // --end->
    }
}
